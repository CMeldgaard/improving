<?php
/**
 * Suntimes - PHP Application for getting sunset and sunup times
 *
 * @author Meldgaard
 * @license http://opensource.org/licenses/MIT MIT License
 */

//Set ROOT constant that holds the path for the project, etv. "/var/www/.
//DIRECTORY_SEPERATOR is used to add a slash at the end of the path.
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);

// Set APP constant that holds the project's "app" folder, like "/var/www/app"
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);

// Composer-dependecies autoloader
require ROOT . 'vendor/autoload.php';

// Load application configuration
require APP . 'config/config.php';

// Load the suntimes application class
use Suntimes\Core\App;

// Start the Suntimes application
$app = new App();
