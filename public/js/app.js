$(function () {

    $("#cityname").geocomplete({ details: "form" });

    if ($('#fetchTimes').length !== 0) {

        $('#fetchTimes').on('click', function () {

            if(!$('#lat').val()){
                return false;
            }

            if(!$('#dateInput').val()){
                return false;
            }

            $('.loader').css('display', 'flex');
            $.ajax({
                type: "POST",
                url: url + "/suntimes/getTimes",
                data: $('form').serialize()
            })
                .done(function (result) {
                    var days = JSON.parse(result);
                    var htmlDays = '';
                    for(var i = 0; i < days.length; i++) {
                        var obj = days[i];
                        htmlDays = htmlDays + '<div class="day">' +
                            '<div class="when">' +
                            '<div class="day-weekday">'+obj.weekday+'</div>' +
                            '<div class="date">'+obj.date+'</div>' +
                            '</div>' +
                            '<div class="times"> ' +
                            '<div class="sunrise"><img src="img/sunrise.svg">'+obj.sunrise+'</div>' +
                            '<div class="sunset"><img src="img/sunset.svg">'+obj.sunset+'</div>' +
                            '</div>' +
                            '</div>';
                    }

                    $('#timesbox').html(htmlDays);
                    $('.loader').css('display', 'none');
                })
                .fail(function () {
                    // this will be executed if the ajax-call had failed
                })
                .always(function () {
                    // this will ALWAYS be executed, regardless if the ajax-call was success or not
                });
        });

    }

});
