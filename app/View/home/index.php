<div class="widget">
    <div class="loader">
        <div class="lds-grid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <h1>Suntimes</h1>
    <div class="subheading">Find sunrise and sunset times around the world</div>
    <input name="cityname" id="cityname">
    <button href="#" id="fetchTimes"><img src="img/search.svg"></button>
    <form>
        <input type="date" id="dateInput" name="startDate" value="<?php echo date("Y-m-j") ?>">
        <input name="lat" type="hidden" value="" id="lat">
        <input name="lng" type="hidden" value="">
    </form>
    <div id="timesbox"></div>
</div>