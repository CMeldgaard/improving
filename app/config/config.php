<?php
/**
 * Configuration for: URL
 * Here we auto-detect the applications URL and the potential sub-folder.
 *
 * URL_PUBLIC_FOLDER:
 * The folder that is visible to public, users will only have access to that folder so nobody can have a look into
 * "/app" or other folder inside the application or call any other .php file than index.php inside "/public".
 *
 * URL_PROTOCOL:
 * The protocol.  The protocol-independent '//' is used, which auto-recognized the protocol.
 *
 * URL_DOMAIN:
 * The domain.
 *
 * URL_SUB_FOLDER:
 * The sub-folder.
 *
 * URL:
 * The final, auto-detected URL (build via the segments above).
 */
define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', '//');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);