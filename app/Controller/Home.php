<?php

namespace Suntimes\Controller;

class Home
{
    /**
     * Renders index URL
     */
    public function index()
    {
        // load views
        require APP . 'View/elements/header.php';
        require APP . 'View/home/index.php';
        require APP . 'View/elements/footer.php';
    }
}