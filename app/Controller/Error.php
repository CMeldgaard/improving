<?php

namespace Suntimes\Controller;

class Error
{
    /**
     * This method handles the error page that will be shown when a route is not found
     */
    public function index()
    {
        // load views
        require APP . 'view/elements/header.php';
        require APP . 'view/error/index.php';
        require APP . 'view/elements/footer.php';
    }
}