<?php

namespace Suntimes\Controller;

use Suntimes\Core\SunriseSet;

class Suntimes
{

    public function index()
    {
        return '';
    }

    /**
     * Returns sunset/rise times from the selected date until the firstcomming sunday
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTimes()
    {
        $suntimes = new SunriseSet();
        $suntimes->setLatLon($_POST['lat'], $_POST['lng']);

        //Check if user has selected a date, if not the use todays date.
        if (isset($_POST['startDate'])) {
            $time = $_POST['startDate'];
            $weekday = (int)date('N', strtotime($time));
        }else {
            $time = date('Y-n-d');
            $weekday = (int)date('N', $time);
        }

        //Loop thru the remaining days of the week, and return the suntimes
        $i = 0;
        $days = [];
        while ($weekday < 8) {

            $suntimes->setDate((new \DateTime($time))->modify('+' . $i . ' day')->format('Y-n-d'));
            $suntimes->request();

            if ($suntimes->isSuccess()) {
                $days[] = $suntimes->getTimes();
                $weekday++;
                $i++;
            }
        }

        //Return the array as json
        echo json_encode($days);
    }

}