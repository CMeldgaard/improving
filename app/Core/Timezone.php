<?php

namespace Suntimes\Core;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class Timezone
{
    protected $client;
    protected $options;
    protected $lat;
    protected $lon;
    protected $response;
    protected $exception;

    public function __construct()
    {
        //Prep Guzzle client
        $this->client = new Client(array('base_uri' => 'http://api.geonames.org/timezoneJSON'));

        //Prep options
        $this->options = [
            'headers' => [
                'accept' => 'application/json'
            ]
        ];
    }

    /**
     * Set lattitude and longitude of requested timezone
     * @param $lat
     * @param $lon
     * @return $this
     */
    public function setLatLon($lat, $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;

        return $this;
    }

    /**
     * Returns information of the timezone, of which the coordinates are a part of
     * @return $this
     */
    public function getTimeZone()
    {
        $this->request();
        return $this;
    }

    /**
     * Returns the ID of the timezone, etc. Europe/Copenhagen
     * @return bool
     */
    public function getTimeZoneId()
    {
        $this->getTimeZone();

        if ($this->isSuccess()) {
            $json = $this->jsonDecode();
            return $json['timezoneId'];
        }

        return false;
    }

    public function request()
    {
        try{
            $this->response = $this->client->request('GET', '?lat=' . $this->lat . '&lng=' . $this->lon . '&username=meldgaard', $this->options);
        }catch (BadResponseException $e){
            $this->exception = true;
            $this->exceptionResponse = $e;
            return $this;
        }

        return $this;
    }

    /**
     * Check if request was successfull
     * @return bool
     */
    public function isSuccess()
    {
        if ($this->exception) {
            return false;
        }

        if ($this->response->getStatusCode() === 200) {
            return true;
        }

        return false;
    }

    /**
     * Converts JSON result into Array
     * @return mixed
     */
    public function jsonDecode()
    {
        return json_decode($this->response->getBody()->getContents(), true);
    }
}