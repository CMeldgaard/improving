<?php

namespace Suntimes\Core;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class SunriseSet
{
    protected $client;
    protected $options;
    protected $lat;
    protected $lon;
    protected $date;
    protected $exception;
    protected $exceptionResponse;
    protected $response;
    protected $json;

    public function __construct()
    {
        //Prep Guzzle client
        $this->client = new Client(array('base_uri' => 'https://api.sunrise-sunset.org/json'));

        //Prep options
        $this->options = [
            'headers' => [
                'accept' => 'application/json'
            ]
        ];
    }

    /**
     * Set date of requested suntimes
     * @param $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set the latitude and longitude of the request
     * @param String $lat
     * @param String $lon
     * @return $this
     */
    public function setLatLon(String $lat, String $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;

        return $this;
    }

    /**
     * Execute the call to the API
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request()
    {
        try{
            $this->response = $this->client->request('GET', '?formatted=0&lat=' . $this->lat . '&lng=' . $this->lon . '&date=' . $this->date, $this->options);
        }catch (BadResponseException $e){
            $this->exception = true;
            $this->exceptionResponse = $e;
            return $this;
        }

        $this->json = $this->jsonDecode();
        return $this;
    }

    /**
     * Returns times for the sunrise, sunset as well as the wwekday and date
     * @return array
     */
    public function getTimes()
    {
        return [
            'sunset'  => $this->getSunset(),
            'sunrise' => $this->getSunrise(),
            'weekday' => date('l', strtotime($this->date)),
            'date'    => date('d-m-Y', strtotime($this->date))
        ];
    }

    /**
     * Return local time of sunset
     * @return string|null
     */
    public function getSunset()
    {
        try{
            $time = new DateTime($this->json['results']['sunset']);
        }catch (\Exception $e){
            return null;
        }

        return $this->toLocal($time);
    }

    /**
     * Returns local time of sunrise
     * @return string|null
     */
    public function getSunrise()
    {
        try{
            $time = new DateTime($this->json['results']['sunrise']);
        }catch (\Exception $e){
            return null;
        }

        return $this->toLocal($time);
    }

    /**
     * @param DateTime $time
     * @return string
     */
    public function toLocal(DateTime $time)
    {
        $timeZone = new Timezone();
        $timeZone->setLatLon($this->lat, $this->lon);
        $timeZoneId = $timeZone->getTimeZoneId();
        $time->setTimezone(new DateTimeZone($timeZoneId));

        return $time->format('H:i');
    }

    /**
     * Check if request was successfull
     * @return bool
     */
    public function isSuccess()
    {
        if ($this->exception) {
            return false;
        }

        if ($this->response->getStatusCode() === 200) {
            return true;
        }

        return false;
    }

    /**
     * Converts JSON result into Array
     * @return mixed
     */
    public function jsonDecode()
    {
        return json_decode($this->response->getBody()->getContents(), true);
    }
}