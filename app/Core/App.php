<?php
/** For more info about namespaces plase @see http://php.net/manual/en/language.namespaces.importing.php */

namespace Suntimes\Core;

class App
{

    private $urlController = null;
    private $urlAction = null;

    /**
     * "Start" the application:
     * Analyze the URL elements and calls the according controller/method or the fallback
     */
    public function __construct()
    {
        //create array with URL parts in $url
        $this->splitUrl();

        // Check if a cobtroller is requested, if not then request the Home controller,
        if (!$this->urlController) {
            $page = new \Suntimes\Controller\Home();
            $page->index();
        }elseif (file_exists(APP . 'Controller/' . ucfirst($this->urlController) . '.php')) {
            // Check of a controller exists. If it does, load the file and create the controller
            $controller = "\\Suntimes\\Controller\\" . ucfirst($this->urlController);
            $this->urlController = new $controller();
            // check for method: does such a method exist in the controller ?
            if (method_exists($this->urlController, $this->urlAction) &&
                is_callable(array($this->urlController, $this->urlAction))) {
                $this->urlController->{$this->urlAction}();
            }else {
                if (strlen($this->urlAction) == 0) {
                    // No action defined in url: call the default index() method of a selected controller
                    $this->urlController->index();
                }else {
                    $page = new \Suntimes\Controller\Error();
                    $page->index();
                }
            }
        }else {
            $page = new \Suntimes\Controller\Error();
            $page->index();
        }
    }

    /**
     * Get and split the URL
     */
    private function splitUrl()
    {
        if (isset($_GET['url'])) {
            // split URL
            $url = trim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            // Put URL parts into according properties
            $this->urlController = isset($url[0]) ? $url[0] : null;
            $this->urlAction = isset($url[1]) ? $url[1] : null;
        }
    }
}