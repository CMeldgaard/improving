#Suntimes
Application to find sunrise and sunset times for locations around the world.

###Uses the following API's:
* Google Places API (Thru Geocomplete.js)
* Geonames to fetch timezone information
* Sunrise-sunset.org to fetch sunrise/set times in UTC times
